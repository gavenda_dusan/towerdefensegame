﻿using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Cinemachine
{
    public class VirtualCameraSettings : MonoBehaviour
    {
        [SerializeField] private float maxCameraDistance;
        [SerializeField] private float minCameraDistance;

        private CinemachineVirtualCamera _virtualCamera;
        private PlayerInput _playerInput;
        private float _zoomDirection;

        private void Awake()
        {
            _virtualCamera = GetComponent<CinemachineVirtualCamera>();
            _playerInput = new PlayerInput();
        }

        private void OnEnable()
        {
            _playerInput.Player.Zoom.Enable();
            _playerInput.Player.Zoom.performed += StartZoomCallback;
            _playerInput.Player.Zoom.canceled += StopZoomCallback;
        }

        private void OnDisable()
        {
            _playerInput.Player.Zoom.Disable();
            _playerInput.Player.Zoom.performed -= StartZoomCallback;
            _playerInput.Player.Zoom.canceled -= StopZoomCallback;
        }

        private void Update()
        {
            HandleZoom();
        }

        private void HandleZoom()
        {
            var cinemachineFramingTransposer = _virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
            if (_zoomDirection > 0)
            {
                if (cinemachineFramingTransposer.m_CameraDistance > minCameraDistance)
                    cinemachineFramingTransposer.m_CameraDistance -= 2f;
                else
                    cinemachineFramingTransposer.m_CameraDistance = minCameraDistance;
            }

            if (_zoomDirection < 0)
            {
                if (cinemachineFramingTransposer.m_CameraDistance < maxCameraDistance)
                    cinemachineFramingTransposer.m_CameraDistance += 2f;
                else
                    cinemachineFramingTransposer.m_CameraDistance = maxCameraDistance;
            }
        }

        private void StartZoomCallback(InputAction.CallbackContext context)
        {
            _zoomDirection = context.ReadValue<Vector2>().y;
        }

        private void StopZoomCallback(InputAction.CallbackContext context)
        {
            _zoomDirection = 0f;
        }

        //private void HandleZoom()
        //{
        //    var cinemachineFramingTransposer = _virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
        //    if (Input.GetAxis(MouseButtons.ScrollWheel) > 0)
        //    {
        //        if (cinemachineFramingTransposer.m_CameraDistance > minCameraDistance)
        //            cinemachineFramingTransposer.m_CameraDistance -= 2f;
        //        else
        //            cinemachineFramingTransposer.m_CameraDistance = minCameraDistance;
        //    }

        //    if (Input.GetAxis(MouseButtons.ScrollWheel) < 0)
        //    {
        //        if (cinemachineFramingTransposer.m_CameraDistance < maxCameraDistance)
        //            cinemachineFramingTransposer.m_CameraDistance += 2f;
        //        else
        //            cinemachineFramingTransposer.m_CameraDistance = maxCameraDistance;
        //    }
        //}
    }
}