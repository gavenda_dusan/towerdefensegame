﻿using Assets.Scripts.Containers;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameHandler : MonoBehaviour
    {
        //public CameraController cameraController;
        //public Transform cameraPoint;
        [SerializeField] private GameObject _prefab;

        [SerializeField] private Transform _terrainParent;
        [SerializeField] private int _columns;
        [SerializeField] private int _rows;

        private float _sizeX;
        private float _sizeZ;
        private bool _areBoundsChecked;

        private void Start()
        {
            //GenerateTerrain();
        }

        private void Update()
        {
            if (!NodeContainer.IsContainerInitialized())
                return;

            if (!_areBoundsChecked)
                GetTerrainBounds();
        }

        private void GenerateTerrain()
        {
            var mesh = _prefab.GetComponent<MeshFilter>().sharedMesh;
            _sizeX = mesh.bounds.size.x * _prefab.transform.localScale.x;
            _sizeZ = mesh.bounds.size.z * _prefab.transform.localScale.z;

            var offsetX = (_columns * _sizeX / 2) - (_sizeX / 2);
            var offsetZ = (_rows * _sizeZ / 2) - (_sizeZ / 2);

            for (int i = 0; i < _columns * _rows; i++)
            {
                var node = Instantiate(_prefab, new Vector3(-offsetX + (_sizeX * (i % _columns)), _prefab.transform.position.y, -offsetZ + (_sizeZ * (i / _columns))), Quaternion.identity, _terrainParent);
                node.gameObject.name = $"Node [{node.transform.position.x}] [{node.transform.position.y}] [{node.transform.position.z}]";
            }
        }

        private void GetTerrainBounds()
        {
            NodeContainer.CalculateNodesSize();
            _areBoundsChecked = true;
        }
    }
}