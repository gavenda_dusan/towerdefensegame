﻿using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class BuildController : MonoBehaviour
    {
        public static BuildController instance;

        [SerializeField] private GameObject objectToBuild;

        private GameObject _buildObject;

        private void Awake()
        {
            if (instance)
            {
                Debug.LogError("Cannot have more than one BuildManger");
                return;
            }

            instance = this;
        }

        private void Start()
        {
            _buildObject = objectToBuild;
        }

        public GameObject GetObjectToBuild()
        {
            return _buildObject;
        }
    }
}