﻿using Assets.Scripts.Enums;
using System;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class CameraController : MonoBehaviour
    {
        public float cameraMoveSpeed;
        public Vector3 cameraOffset;
        public float maxZoom;
        public float minZoom;

        private Func<Vector3> GetCameraFollowPositionFunc;
        private Vector3 centeredCam;

        public void Setup(Func<Vector3> GetCameraFollowPositionFunc)
        {
            this.GetCameraFollowPositionFunc = GetCameraFollowPositionFunc;
        }

        private void Start()
        {
            centeredCam = transform.GetComponent<Camera>().transform.position;
        }

        private void Update()
        {
            HandleMovement();
            HandleZoom();
        }

        private void HandleMovement()
        {
            CalculateSmoothMovement(GetCameraFollowPositionFunc());
        }

        private void HandleZoom()
        {
            //if (Input.GetAxis(MouseButtons.ScrollWheel) > 0)
            //{
            //    if (minZoom < CalculateCameraDistance())
            //        cameraOffset -= centeredCam - CalculateZoomPosition(1f);
            //}

            //if (Input.GetAxis(MouseButtons.ScrollWheel) < 0)
            //{
            //    if (maxZoom > CalculateCameraDistance())
            //        cameraOffset -= centeredCam - CalculateZoomPosition(-1f);
            //}

            CalculateSmoothMovement(CalculateZoomPosition(1f));
        }

        private Vector3 CalculateZoomPosition(float direction)
        {
            return Vector3.MoveTowards(centeredCam, GetCameraFollowPositionFunc(), direction);
        }

        private float CalculateCameraDistance()
        {
            return Vector3.Distance(GetCameraFollowPositionFunc(), centeredCam);
        }

        private void CalculateSmoothMovement(Vector3 positionToMoveTo)
        {
            centeredCam = positionToMoveTo + cameraOffset;

            var cameraFollowDirection = (centeredCam - transform.position).normalized;
            var distance = Vector3.Distance(centeredCam, transform.position);
            if (distance > 0)
            {
                var newCameraPosition = transform.position + cameraFollowDirection * distance * cameraMoveSpeed * Time.deltaTime;

                var distanceAfterMoving = Vector3.Distance(newCameraPosition, centeredCam);

                if (distanceAfterMoving > distance)
                {
                    newCameraPosition = centeredCam;
                }

                transform.position = newCameraPosition;
            }
        }
    }
}