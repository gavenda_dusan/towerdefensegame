﻿using Assets.Scripts.Enums;
using Assets.Scripts.GameObjects;
using System;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class IdleState : BaseState
    {
        private Vector3? _destination;
        private Vector3 _direction;
        private Quaternion _desiredRotation;
        private Unit _unit;
        private float _turnSpeed = 10f;
        private float _stopDistance = 1f;
        private readonly LayerMask _layerMask = LayerMask.GetMask("Environment");
        private float _rayDistance = 1f;
        private float _timer = 0f;
        private float _maxWaitTime = 5f;

        public IdleState(Unit unit) : base(unit.gameObject, UnitStates.Idle)
        {
            _unit = unit;
        }

        public override UnitStates Tick()
        {
            if (_unit.BuildTargetNode != null)
                return UnitStates.MovingTowards;

            if (!_destination.HasValue)
                FindRandomDestination();

            if (Vector3.Distance(transform.position, _destination.Value) <= _stopDistance)
            {
                if (_timer < _maxWaitTime)
                {
                    _timer += Time.deltaTime;
                    return StateName;
                }
                else
                {
                    _timer = 0f;
                    FindRandomDestination();
                }
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, _desiredRotation, Time.deltaTime * _turnSpeed);

            if (IsForwardBlocked())
                FindRandomDestination();
            else
                transform.position = Vector3.MoveTowards(transform.position, _destination.Value, Time.deltaTime * _unit.Speed);

            return StateName;
        }

        private bool IsPathBlocked()
        {
            var ray = new Ray(transform.position, _direction);
            return Physics.SphereCast(ray, 0.5f, _rayDistance, _layerMask);
        }

        private bool IsForwardBlocked()
        {
            var ray = new Ray(transform.position, transform.forward);
            return Physics.SphereCast(ray, 0.5f, _rayDistance, _layerMask);
        }

        private void FindRandomDestination()
        {
            var position = transform.position + new Vector3(UnityEngine.Random.Range(-10f, 10f), 0f, UnityEngine.Random.Range(-10f, 10f));
            _destination = new Vector3(position.x, 0f, position.z);
            _direction = Vector3.Normalize(_destination.Value - transform.position);
            _direction = new Vector3(_direction.x, 0f, _direction.z);
            _desiredRotation = Quaternion.LookRotation(_direction);
        }
    }
}