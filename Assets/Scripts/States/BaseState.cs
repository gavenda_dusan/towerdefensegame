﻿using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts.States
{
    public abstract class BaseState
    {
        protected GameObject gameObject;
        protected Transform transform;

        public UnitStates StateName { get; protected set; }

        protected BaseState(GameObject gameObject, UnitStates unitState)
        {
            this.gameObject = gameObject;
            this.transform = gameObject.transform;
            this.StateName = unitState;
        }

        public abstract UnitStates Tick();
    }
}