﻿using Assets.Scripts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class StateMachine : MonoBehaviour
    {
        private Dictionary<UnitStates, BaseState> _availableStates;

        public BaseState CurrentState { get; private set; }

        public event Action<BaseState> OnStateChanged;

        public void SetState(Dictionary<UnitStates, BaseState> states)
        {
            _availableStates = states;
        }

        private void Update()
        {
            if (CurrentState == null)
            {
                CurrentState = _availableStates.Values.First();
            }

            var nextState = CurrentState?.Tick();

            if (nextState != null && nextState != CurrentState.StateName)
            {
                SwitchToNewState(nextState.Value);
            }
        }

        private void SwitchToNewState(UnitStates nextState)
        {
            CurrentState = _availableStates[nextState];
            OnStateChanged?.Invoke(CurrentState);
        }
    }
}