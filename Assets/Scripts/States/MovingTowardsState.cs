﻿using Assets.Scripts.Enums;
using Assets.Scripts.GameObjects;
using System;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class MovingTowardsState : BaseState
    {
        private Vector3? _destination;
        private Vector3 _direction;
        private Quaternion _desiredRotation;
        private Unit _unit;
        private float _turnSpeed = 10f;
        private float _stopDistance = 1f;

        public MovingTowardsState(Unit unit) : base(unit.gameObject, UnitStates.MovingTowards)
        {
            _unit = unit;
        }

        public override UnitStates Tick()
        {
            CheckUnitTargetPosition();
            if (_destination.HasValue)
            {
                if (Vector3.Distance(_destination.Value, transform.position) <= _stopDistance)
                {
                    _destination = null;
                    return UnitStates.Constructing;
                }
                transform.rotation = Quaternion.Slerp(transform.rotation, _desiredRotation, Time.deltaTime * _turnSpeed);
                transform.position = Vector3.MoveTowards(transform.position, _destination.Value, Time.deltaTime * _unit.Speed);
            }
            else
                return UnitStates.Idle;

            return StateName;
        }

        private void CheckUnitTargetPosition()
        {
            if (_unit.BuildTargetNode != null && !_destination.HasValue)
            {
                _destination = new Vector3(_unit.BuildTargetNode.transform.position.x, 0f, _unit.BuildTargetNode.transform.position.z);
                _direction = Vector3.Normalize(_destination.Value - transform.position);
                _direction = new Vector3(_direction.x, 0f, _direction.z);
                _desiredRotation = Quaternion.LookRotation(_direction);
            }
        }
    }
}