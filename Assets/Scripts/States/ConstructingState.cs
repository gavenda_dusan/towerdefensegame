﻿using Assets.Scripts.Enums;
using Assets.Scripts.GameObjects;

namespace Assets.Scripts.States
{
    public class ConstructingState : BaseState
    {
        private Unit _unit;

        public ConstructingState(Unit unit) : base(unit.gameObject, UnitStates.Constructing)
        {
            _unit = unit;
        }

        public override UnitStates Tick()
        {
            var constuctionState = _unit.BuildTargetNode.ConstructionState;

            if (constuctionState == ConstructionStates.NotStarted)
                StartConstruction();

            if (constuctionState == ConstructionStates.Completed)
            {
                _unit.BuildTargetNode = null;
                return UnitStates.Idle;
            }

            return StateName;
        }

        private void StartConstruction()
        {
            _unit.BuildTargetNode.StartConstruction();
        }
    }
}