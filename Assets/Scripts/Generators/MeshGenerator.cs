﻿using UnityEngine;

namespace Assets.Scripts.Generators
{
    public class MeshGenerator : MonoBehaviour
    {
        private Vector3[] vertices;
        private int[] triangles;
        private Mesh mesh;
        private int xSize = 100;
        private int zSize = 100;

        private void Start()
        {
            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;

            CreateShape();
            UpdateMesh();
        }

        private void CreateShape()
        {
            vertices = new Vector3[(xSize + 1) * (zSize + 1)];

            for (int i = 0, z = 0; z <= zSize; z++)
            {
                for (int x = 0; x <= xSize; x++)
                {
                    var y = 1 + Mathf.PerlinNoise(x * 0.5f, z * 0.5f) * 2f;
                    vertices[i] = new Vector3(x, y, z);
                    i++;
                }
            }

            triangles = new int[xSize * zSize * 6];
            var verts = 0;
            var tries = 0;

            for (int z = 0; z < zSize; z++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    triangles[tries] = verts;
                    triangles[tries + 1] = verts + xSize + 1;
                    triangles[tries + 2] = verts + 1;
                    triangles[tries + 3] = verts + 1;
                    triangles[tries + 4] = verts + xSize + 1;
                    triangles[tries + 5] = verts + xSize + 2;

                    verts++;
                    tries += 6;
                }
                verts++;
            }
        }

        private void UpdateMesh()
        {
            mesh.Clear();

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
        }

        private void OnDrawGizmos()
        {
            if (vertices == null)
                return;

            for (int i = 0; i < vertices.Length; i++)
            {
                Gizmos.DrawSphere(vertices[i], 0.1f);
            }
        }
    }
}