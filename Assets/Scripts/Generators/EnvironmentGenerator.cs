﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Generators
{
    public class EnvironmentGenerator : MonoBehaviour
    {
        private void Awake()
        {
            var listOfPositions = new List<Vector3>
            {
                new Vector3(15f, 0f ,5f),
                new Vector3(15f, 0f ,2f),
                new Vector3(15f, 0f ,1f),
                new Vector3(15f, 0f ,-10f),
                new Vector3(23f, 0f ,-10f),
                new Vector3(24f, 0f ,-10f),
                new Vector3(33f, 0f ,-10f),
                new Vector3(27f, 0f ,5f),
                new Vector3(23f, 0f ,5f),
                new Vector3(15f, 0f ,5f)
            };
            GenerateRocks(listOfPositions);

            listOfPositions = new List<Vector3>
            {
                new Vector3(13f, 0f ,-4f),
                new Vector3(13f, 0f ,-5f),
                new Vector3(14f, 0f ,-5f),
                new Vector3(15f, 0f ,-5f),
                new Vector3(16f, 0f ,-5f),
                new Vector3(16f, 0f ,-4f),
                new Vector3(15f, 0f ,-4f),
                new Vector3(20f, 0f ,-8f)
            };
            GenerateRocks(listOfPositions);
        }

        private void GenerateRocks(List<Vector3> listOfPositions)
        {
            var initPosition = listOfPositions[0];

            var minX = initPosition.x;
            var maxX = initPosition.x;
            var minZ = initPosition.z;
            var maxZ = initPosition.z;

            foreach (Vector3 position in listOfPositions)
            {
                if (minX > position.x)
                    minX = position.x;

                if (maxX < position.x)
                    maxX = position.x;

                if (minZ > position.z)
                    minZ = position.z;

                if (maxZ < position.z)
                    maxZ = position.z;
            }

            minX -= 0.5f;
            maxX += 0.5f;
            minZ -= 0.5f;
            maxZ += 0.5f;

            var distanceX = maxX - minX;
            var distanceZ = maxZ - minZ;

            var rock = Resources.Load<GameObject>("Rocks/GroundLevel/rock_plane_2");

            var bounds = rock.GetComponent<MeshRenderer>().bounds;

            var size = bounds.max - bounds.min;
            var scaleX = distanceX / size.x;
            var scaleZ = distanceZ / size.z;
            var scaleY = (scaleX + scaleZ) / 2;

            var random = new System.Random();
            var rotationMultiplier = random.Next(0, 3);

            if (rotationMultiplier == 1 || rotationMultiplier == 3)
            {
                var temp = scaleX;
                scaleX = scaleZ;
                scaleZ = temp;
            }

            var center = new Vector3((minX + maxX) / 2, -bounds.min.y * scaleY, (minZ + maxZ) / 2);

            var model = Instantiate(rock, center, Quaternion.Euler(0f, 90f * rotationMultiplier, 0f));
            model.transform.localScale = new Vector3(model.transform.localScale.x * scaleX, model.transform.localScale.y * scaleY, model.transform.localScale.z * scaleZ);
        }
    }
}