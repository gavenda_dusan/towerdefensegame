﻿namespace Assets.Scripts.Enums
{
    public enum UnitStates
    {
        Idle,
        Constructing,
        MovingTowards
    }
}