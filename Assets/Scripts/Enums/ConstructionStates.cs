﻿namespace Assets.Scripts.Enums
{
    public enum ConstructionStates
    {
        NotStarted,
        InProgress,
        Completed
    }
}