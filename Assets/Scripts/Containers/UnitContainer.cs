﻿using Assets.Scripts.Enums;
using Assets.Scripts.GameObjects;
using Assets.Scripts.States;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Containers
{
    public static class UnitContainer
    {
        private static List<Unit> _units;

        public static void AddUnit(Unit unit)
        {
            if (_units == null)
                _units = new List<Unit>();

            _units.Add(unit);
        }

        public static Unit GetClosest(Vector3 worldPosition)
        {
            var closest = _units.First();
            foreach (var unit in _units)
            {
                var currentDistance = Vector3.Distance(closest.gameObject.transform.position, worldPosition);
                var newDistance = Vector3.Distance(unit.gameObject.transform.position, worldPosition);

                if (newDistance < currentDistance)
                    closest = unit;
            }

            return closest;
        }

        public static Unit GetClosestIdle(Vector3 worldPosition)
        {
            var closest = _units.Where(x => x.StateMachine.CurrentState.StateName == UnitStates.Idle).FirstOrDefault();
            foreach (var unit in _units)
            {
                if (unit.StateMachine.CurrentState.StateName != UnitStates.Idle)
                    continue;

                var currentDistance = Vector3.Distance(closest.gameObject.transform.position, worldPosition);
                var newDistance = Vector3.Distance(unit.gameObject.transform.position, worldPosition);

                if (newDistance < currentDistance)
                    closest = unit;
            }

            //if (closest.StateMachine.CurrentState.StateName != UnitStates.Idle)
            //    return null;

            return closest;
        }
    }
}