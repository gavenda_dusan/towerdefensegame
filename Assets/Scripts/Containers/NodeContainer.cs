﻿using Assets.Scripts.GameObjects;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Containers
{
    public static class NodeContainer
    {
        public static Vector3 nodeMinimalBounds;
        public static Vector3 nodeMaximalBounds;

        private static List<Node> _nodes;

        public static void AddNode(Node node)
        {
            if (_nodes == null)
                _nodes = new List<Node>();

            _nodes.Add(node);
        }

        public static bool IsContainerInitialized()
        {
            if (_nodes == null)
                return false;
            return true;
        }

        public static void CalculateNodesSize()
        {
            var initPosition = _nodes.Select(x => x.transform.position).First();
            var initMeshBounds = _nodes.Select(x => x.GetComponent<MeshFilter>().mesh.bounds).First();
            var maxX = initPosition.x + initMeshBounds.min.x;
            var minX = initPosition.x + initMeshBounds.max.x;
            var maxZ = initPosition.z + initMeshBounds.min.z;
            var minZ = initPosition.z + initMeshBounds.max.z;

            foreach (Node node in _nodes)
            {
                var localMinX = node.transform.position.x + node.GetComponent<MeshFilter>().mesh.bounds.min.x;
                var localMaxX = node.transform.position.x + node.GetComponent<MeshFilter>().mesh.bounds.max.x;
                var localMinZ = node.transform.position.z + node.GetComponent<MeshFilter>().mesh.bounds.min.z;
                var localMaxZ = node.transform.position.z + node.GetComponent<MeshFilter>().mesh.bounds.max.z;

                if (minX > localMinX)
                    minX = localMinX;

                if (maxX < localMaxX)
                    maxX = localMaxX;

                if (minZ > localMinZ)
                    minZ = localMinZ;

                if (maxZ < localMaxZ)
                    maxZ = localMaxZ;
            }

            nodeMinimalBounds = new Vector3(minX, 0, minZ);
            nodeMaximalBounds = new Vector3(maxX, 0, maxZ);
        }
    }
}