﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Containers
{
    public static class BuildContainer
    {
        private static List<Vector3> _buildPositions;
        private static List<Vector3> _reservedBuildPositions;

        public static void AddBuildPosition(Vector3 position)
        {
            if (_buildPositions == null)
                _buildPositions = new List<Vector3>();

            _buildPositions.Add(position);
        }

        public static void ReserveBuildPosition(int index)
        {
            _reservedBuildPositions.Add(_buildPositions[index]);
            _buildPositions.RemoveAt(index);
        }
    }
}