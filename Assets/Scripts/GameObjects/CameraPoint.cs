﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.GameObjects
{
    public class CameraPoint : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private bool _enableEdgeScrolling;

        private PlayerInput _playerInput;
        private Vector2 _moveAxis;

        private void Awake()
        {
            _playerInput = new PlayerInput();
        }

        private void OnEnable()
        {
            _playerInput.Player.Move.Enable();
            _playerInput.Player.Move.performed += StartMoveCallback;
            _playerInput.Player.Move.canceled += StopMoveCallback;
        }

        private void OnDisable()
        {
            _playerInput.Player.Move.Disable();
            _playerInput.Player.Move.performed -= StartMoveCallback;
            _playerInput.Player.Move.canceled -= StopMoveCallback;
        }

        private void Update()
        {
            //MoveByKeyboard();
            //if (enableEdgeScrolling)
            //MoveByMouse();
            Move();
        }

        private void StartMoveCallback(InputAction.CallbackContext context)
        {
            _moveAxis = context.ReadValue<Vector2>();
        }

        private void StopMoveCallback(InputAction.CallbackContext context)
        {
            _moveAxis = new Vector2();
        }

        private void Move()
        {
            transform.Translate(_moveAxis.x * _speed * Time.deltaTime, 0, _moveAxis.y * _speed * Time.deltaTime);
        }

        //private void MoveByKeyboard()
        //{
        //    if (Input.GetKey(KeyCode.W))
        //        transform.Translate(0, 0, speed * Time.deltaTime);

        //    if (Input.GetKey(KeyCode.S))
        //        transform.Translate(0, 0, -speed * Time.deltaTime);

        //    if (Input.GetKey(KeyCode.A))
        //        transform.Translate(-speed * Time.deltaTime, 0, 0);

        //    if (Input.GetKey(KeyCode.D))
        //        transform.Translate(speed * Time.deltaTime, 0, 0);
        //}

        //private void MoveByMouse()
        //{
        //    var edgeSize = 20f;

        //    if (Input.mousePosition.x > Screen.width - edgeSize)
        //        transform.Translate(speed * Time.deltaTime, 0, 0);

        //    if (Input.mousePosition.x < edgeSize)
        //        transform.Translate(-speed * Time.deltaTime, 0, 0);

        //    if (Input.mousePosition.y > Screen.height - edgeSize)
        //        transform.Translate(0, 0, speed * Time.deltaTime);

        //    if (Input.mousePosition.y < edgeSize)
        //        transform.Translate(0, 0, -speed * Time.deltaTime);
        //}
    }
}