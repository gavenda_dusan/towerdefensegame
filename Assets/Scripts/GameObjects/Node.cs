﻿using Assets.Scripts.Containers;
using Assets.Scripts.Controllers;
using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts.GameObjects
{
    public class Node : MonoBehaviour
    {
        [SerializeField] private Color hoverColor;
        [SerializeField] private GameObject buildingText;

        private Color _startColor;
        private Renderer _renderer;
        private GameObject _gameObject;
        private GameObject _buildingTextInstance;
        private Unit _currentBuilder;

        private int _constructionTicks = 0;
        private int _constructionTicksMax = 3;
        public ConstructionStates ConstructionState { get; private set; }

        private void Start()
        {
            ConstructionState = ConstructionStates.NotStarted;
            NodeContainer.AddNode(this);
            _renderer = GetComponent<Renderer>();
            _startColor = _renderer.material.color;
        }

        private void OnMouseEnter()
        {
            _renderer.material.color = hoverColor;
        }

        private void OnMouseExit()
        {
            _renderer.material.color = _startColor;
        }

        private void OnMouseDown()
        {
            if (_gameObject)
            {
                Debug.Log("Node is occupied");
            }
            else
            {
                if (_constructionTicks < _constructionTicksMax && _buildingTextInstance == null)
                {
                    InvokeRepeating("QueueConstruction", 0f, 1f);
                }
            }
        }

        private void QueueConstruction()
        {
            if (_currentBuilder == null)
                _currentBuilder = UnitContainer.GetClosestIdle(transform.position);
            if (_currentBuilder != null)
                _currentBuilder.BuildTargetNode = this;
        }

        public void StartConstruction()
        {
            ConstructionState = ConstructionStates.InProgress;
            ShowBuildingText();
            InvokeRepeating("IncreaseConstructionTicks", 1f, 1f);
        }

        private void IncreaseConstructionTicks()
        {
            if (_constructionTicks == _constructionTicksMax)
            {
                DestroyBuildingText();
                BuildCube();
                _constructionTicks = 0;
                CancelInvoke("IncreaseConstructionTicks");
            }
            else
            {
                _constructionTicks++;
            }
        }

        private void BuildCube()
        {
            var objectToBuild = BuildController.instance.GetObjectToBuild();
            var offsetY = transform.GetComponent<MeshFilter>().mesh.bounds.max.y;

            _gameObject = Instantiate(objectToBuild, transform.position + new Vector3(0, offsetY, 0), transform.rotation);
            ConstructionState = ConstructionStates.Completed;
            CancelInvoke("QueueConstruction");
        }

        private void ShowBuildingText()
        {
            var offsetY = transform.GetComponent<MeshFilter>().mesh.bounds.max.y + 1f;
            _buildingTextInstance = Instantiate(buildingText, transform.position + new Vector3(0, offsetY, 0), transform.rotation);
        }

        private void DestroyBuildingText()
        {
            Destroy(_buildingTextInstance);
        }
    }
}