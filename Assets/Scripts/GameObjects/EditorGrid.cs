﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.GameObjects
{
    public class EditorGrid : MonoBehaviour
    {
        private int _width = 30;
        private int _height = 30;
        private float _cellSize = 1f;
        private Color[,] _gridArray;
        private SpriteRenderer[,] _debugTextArray;
        private PlayerInput _playerInput;
        private Vector3 _mousePosition;
        private Vector3 _originPosition = new Vector3(0, 0, 0);

        private void Awake()
        {
            _gridArray = new Color[_width, _height];
            _debugTextArray = new SpriteRenderer[_width, _height];
            _playerInput = new PlayerInput();
            DrawGrid();
        }

        private void OnEnable()
        {
            _playerInput.UI.Click.Enable();
            _playerInput.UI.Click.performed += StartClickCallback;
            _playerInput.UI.Click.canceled += StopClickCallback;

            _playerInput.UI.RightClick.Enable();
            _playerInput.UI.RightClick.performed += StartRightClickCallback;

            _playerInput.UI.Point.Enable();
            _playerInput.UI.Point.performed += StartPositionTrackingCallback;
        }

        private void OnDisable()
        {
            _playerInput.UI.Click.Disable();
            _playerInput.UI.Click.performed -= StartClickCallback;
            _playerInput.UI.Click.canceled -= StopClickCallback;

            _playerInput.UI.RightClick.Disable();
            _playerInput.UI.RightClick.performed -= StartRightClickCallback;

            _playerInput.UI.Point.Disable();
            _playerInput.UI.Point.performed -= StartPositionTrackingCallback;
        }

        private void StartRightClickCallback(InputAction.CallbackContext context)
        {
            //Debug.Log(GetValue(_mousePosition));
        }

        private void StartPositionTrackingCallback(InputAction.CallbackContext context)
        {
            var vec2 = context.ReadValue<Vector2>();
            var test = new Vector3(vec2.x, vec2.y, Camera.main.transform.position.y);
            _mousePosition = Camera.main.ScreenToWorldPoint(test);
        }

        private void StartClickCallback(InputAction.CallbackContext context)
        {
            SetValue(_mousePosition, Color.red);
        }

        private void StopClickCallback(InputAction.CallbackContext context)
        {
        }

        private Vector3 GetWorldPosition(int x, int z)
        {
            return new Vector3(x, 0, z) * _cellSize + _originPosition;
        }

        private void GetXZ(Vector3 worldPosition, out int x, out int z)
        {
            x = Mathf.FloorToInt((worldPosition - _originPosition).x / _cellSize);
            z = Mathf.FloorToInt((worldPosition - _originPosition).z / _cellSize);
        }

        private void AddIndicies(int indiciesCounter, List<int> indicies, out int indiciesCount)
        {
            for (int i = 0; i < 4; i++)
            {
                indicies.Add(indiciesCounter++);
            }
            indiciesCount = indiciesCounter;
        }

        private void SetValue(int x, int z, Color value)
        {
            if (x >= 0 && z >= 0 && x < _width && z < _height)
            {
                _gridArray[x, z] = value;
                //_debugTextArray[x, z].color = _gridArray[x, z];
            }
        }

        private void SetValue(Vector3 worldPosition, Color value)
        {
            int x, z;
            GetXZ(worldPosition, out x, out z);
            SetValue(x, z, value);
        }

        //private int GetValue(int x, int z)
        //{
        //    if (x >= 0 && z >= 0 && x < _width && z < _height)
        //    {
        //        return _gridArray[x, z];
        //    }
        //    else
        //    {
        //        return -1;
        //    }
        //}

        //private int GetValue(Vector3 worldPosition)
        //{
        //    int x, z;
        //    GetXZ(worldPosition, out x, out z);
        //    return GetValue(x, z);
        //}

        private void DrawGrid()
        {
            var filter = gameObject.GetComponent<MeshFilter>();
            var mesh = new Mesh();
            var verticies = new List<Vector3>();
            var indicies = new List<int>();
            var indiciesCounter = 0;

            for (int x = 0; x < _gridArray.GetLength(0); x++)
            {
                for (int z = 0; z < _gridArray.GetLength(1); z++)
                {
                    //var gameObject = new GameObject("World_Text");
                    //var renderer = gameObject.AddComponent<SpriteRenderer>();
                    //renderer.color = Color.white;
                    //renderer.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
                    ////renderer.sprite = Sprite.Create(new Texture2D(1, 1), new Rect(), new Vector2());
                    //gameObject.transform.position = GetWorldPosition(x, z) + new Vector3(_cellSize, 0, _cellSize) * 0.5f;
                    //gameObject.transform.Rotate(new Vector3(90f, 0f, 0f));

                    //var size = renderer.bounds.max - renderer.bounds.min;
                    //var scaleX = 1f / size.x;
                    //var scaleZ = 1f / size.z;
                    //gameObject.transform.localScale = new Vector3(1f * scaleX, 1f * scaleZ, 1f);

                    //_debugTextArray[x, z] = renderer;

                    verticies.Add(GetWorldPosition(x, z));
                    verticies.Add(GetWorldPosition(x, z + 1));
                    verticies.Add(GetWorldPosition(x, z));
                    verticies.Add(GetWorldPosition(x + 1, z));

                    AddIndicies(indiciesCounter, indicies, out indiciesCounter);
                }
            }

            verticies.Add(GetWorldPosition(0, _height));
            verticies.Add(GetWorldPosition(_width, _height));
            verticies.Add(GetWorldPosition(_width, 0));
            verticies.Add(GetWorldPosition(_width, _height));

            AddIndicies(indiciesCounter, indicies, out indiciesCounter);

            mesh.vertices = verticies.ToArray();
            mesh.SetIndices(indicies.ToArray(), MeshTopology.Lines, 0);
            filter.mesh = mesh;

            var meshRenderer = gameObject.GetComponent<MeshRenderer>();
            meshRenderer.material = new Material(Shader.Find("Sprites/Default"));
            meshRenderer.material.color = Color.white;
        }
    }
}