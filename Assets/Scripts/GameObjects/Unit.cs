﻿using Assets.Scripts.Containers;
using Assets.Scripts.Enums;
using Assets.Scripts.States;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.GameObjects
{
    public class Unit : MonoBehaviour
    {
        public float Speed { get; private set; }
        public Node BuildTargetNode { get; set; }

        public StateMachine StateMachine => GetComponent<StateMachine>();

        private void Awake()
        {
            Speed = 2f;
            UnitContainer.AddUnit(this);
            InitialiseStateMachine();
        }

        private void InitialiseStateMachine()
        {
            var states = new Dictionary<UnitStates, BaseState>()
            {
            { UnitStates.Idle, new IdleState(this)},
            { UnitStates.MovingTowards, new MovingTowardsState(this)},
            { UnitStates.Constructing, new ConstructingState(this)}
            };

            StateMachine.SetState(states);
        }

        private void Update()
        {
            var text = GetComponentInChildren<TextMeshPro>();
            text.text = StateMachine.CurrentState.StateName.ToString();
        }
    }
}